upstream makaneyyat.org {
    server web:8000;
}

server {
    listen 80;
    charset utf-8;
    server_name www.makaneyyat.org makaneyyat.org;

    location ~ /.well-known/acme-challenge{
        allow all;
        root /data/letsencrypt;
    }

    location / {
        return 301 https://makaneyyat.org$request_uri;
    }
}

#https://www.makaneyyat.org
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name www.makaneyyat.org;

    server_tokens off;

    ssl_certificate /etc/letsencrypt/live/makaneyyat.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/makaneyyat.org/privkey.pem;

    ssl_buffer_size 8k;

    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;

    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8;

    return 301 https://makaneyyat.org$request_uri;
}

#https://makaneyyat.org
server {
    server_name makaneyyat.org;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_tokens off;

    ssl_buffer_size 8k;
    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8 8.8.4.4;

    ssl_certificate /etc/letsencrypt/live/makaneyyat.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/makaneyyat.org/privkey.pem;


    location / {
        proxy_pass http://makaneyyat.org;
        proxy_ssl_server_name on;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
    }

    client_max_body_size 4G;
    access_log /code/logs/nginx-access.log;
    error_log /code/logs/nginx-error.log;


    location /static/ {
        alias /code/static/;
    }

    location /media/ {
        alias /code/media/;
    }
}

